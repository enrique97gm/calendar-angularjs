var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope) {

    $scope.currentDate = new Date();
    $scope.selectedYear = $scope.currentDate.getFullYear();
    $scope.selectedMonth = $scope.currentDate.getMonth();

    $scope.calendar = [];
    $scope.tasks = [];

    $scope.newTask = false;

    $scope.months = [
        {
            id: 0,
            text: "January",
            days: 31
        },
        {
            id: 1,
            text: "February",
            days: 28
        },
        {
            id: 2,
            text: "March",
            days: 31
        },
        {
            id: 3,
            text: "April",
            days: 30
        },
        {
            id: 4,
            text: "May",
            days: 31
        },
        {
            id: 5,
            text: "June",
            days: 30
        },
        {
            id: 6,
            text: "July",
            days: 31
        },
        {
            id: 7,
            text: "August",
            days: 31
        },
        {
            id: 8,
            text: "September",
            days: 30
        },
        {
            id: 9,
            text: "October",
            days: 31
        },
        {
            id: 10,
            text: "November",
            days: 30
        },
        {
            id: 11,
            text: "December",
            days: 31
        }
    ];

    $scope.daysHeader = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    $scope.colours = ["blue", "red", "green", "orange"];

    $scope.openCloseModal = function(val){
        if(val == 0){
            $scope.newTask = false;
        } else {
            setTimeout(() => {
                $("#startDate").datepicker({
                      dateFormat: 'yy/mm/dd'
                });
                $("#endDate").datepicker({
                    dateFormat: 'yy/mm/dd'
              });
            },50);
            $scope.newTask = true;
        }
    }

    $scope.changeYear = function(val){
        if(val == 0){
            $scope.selectedYear--;
        } else {
            $scope.selectedYear++;
        }
        $scope.createCalendar();
    }

    $scope.changeMonth = function(val){
        if(val == 0){
            if($scope.selectedMonth == 0){
                $scope.selectedMonth = 11;
                $scope.selectedYear--;
            } else{
                $scope.selectedMonth--;
            }
        } else {
            if($scope.selectedMonth == 11){
                $scope.selectedMonth = 0;
                $scope.selectedYear++;
            } else{
                $scope.selectedMonth++;
            }
        }
        if($scope.selectedMonth == 1){
            $scope.months[$scope.selectedMonth].days = GetfebDays();
        }
        $scope.createCalendar();
    }

    function GetfebDays(){
        if ((($scope.selectedYear % 4 == 0) && ($scope.selectedYear % 100 != 0 )) || ($scope.selectedYear % 400 == 0)){
            return 29;
        }
        return 28;
    }

    $scope.createCalendar = function(){
        let date  = new Date($scope.selectedYear, $scope.selectedMonth);
        let dayWeek;
        if(date.getDay() == 0){
            dayWeek = 7;
        } else {
            dayWeek = date.getDay();
        }
        $scope.calendar = [];
        let week = [];
        for(let day = 1; day < $scope.months[$scope.selectedMonth].days+dayWeek; day++){
            if(day%7==0){
                week.push(day-dayWeek+1);
                $scope.calendar.push(week);
                week = [];
            } else {
                if(day < dayWeek){
                    week.push(" ");
                } else{
                    week.push(day-dayWeek+1);
                }
            }  
        }
        $scope.calendar.push(week);
    }

    $scope.isCurrentDay = function(day){
        if($scope.currentDate.getFullYear() == $scope.selectedYear){
            if($scope.currentDate.getMonth() == $scope.selectedMonth){
                if($scope.currentDate.getDate() == day) return true;
            }
        }
        return false;
    }

    $scope.addTask = function(){
        if($('#startDate').val() == ''){
            alertify.alert("Select the start date");
            return;
        };
        if($('#endDate').val() == ''){
            alertify.alert("Select the end date");
            return;
        };
        let startDate = new Date($('#startDate').val());
        let endDate = new Date($('#endDate').val());
        if(endDate < startDate){
            alertify.alert("The end date has to be older than the start date");
            return;
        }
        let task = {
            id: $scope.tasks.length,
            start: startDate,
            end: endDate,
            text: $('#textTask').val(),
            colour: $('#colour').val()
        };
        $scope.tasks.push(task);
        $scope.openCloseModal(0);
        $scope.createCalendar();
        alertify.success('Task created correctly'); 
    }

    $scope.getTasksFromDay = function(day){
        let dateDay = new Date($scope.selectedYear, $scope.selectedMonth, day);
        let tasksDay = $scope.tasks.filter(t => t.start <= dateDay && t.end >= dateDay );
        return tasksDay;
    }
   
    $scope.createCalendar();

});